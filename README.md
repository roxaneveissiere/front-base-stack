# README : Repo Front Base Stack #

Voici le manuel d'utilisation du pack de démarrage front !
Il vous aidera à mettre en place votre projet en vous évitant des copier-coller fastidieux et une perte de temps certaine.

Il suffit de cloner le contenu du repo dans votre dossier d'assets

### Que contient ce repo ? ###

* des mixins
* les settings foundation à jour (modifiés et utilisables pour compiler votre version du Framework)
* le fichier appelant tous les composants (modifiés et utilisables pour compiler votre version du Framework)
* app.scss qui contiendra les styles Foundation
* un fichier de base, 'layout.scss' à dupliquer et renommer pour les besoins de votre projet
* app.js qui contiendra les imports de base et l'initialisation de Foundation

### Et si ça ne fonctionne pas ? ###

* C'est ma faute, venez m'engueuler.